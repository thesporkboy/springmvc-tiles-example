
# Create a database called smvctiles and populate it with this script.
#
# SQL Export
# Created by Querious (945)
# Created: February 6, 2015 at 11:23:35 AM PST
# Encoding: Unicode (UTF-8)
#

#TODO: Get rid of CONTACTS
DROP TABLE IF EXISTS `user_roles`;
DROP TABLE IF EXISTS `users`;
DROP TABLE IF EXISTS `servers`;
DROP TABLE IF EXISTS `CONTACTS`;


CREATE TABLE `CONTACTS` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(45) NOT NULL,
  `address` varchar(45) DEFAULT NULL,
  `gender` char(1) DEFAULT 'M',
  `dob` datetime DEFAULT NULL,
  `email` varchar(45) DEFAULT NULL,
  `mobile` varchar(15) DEFAULT NULL,
  `phone` varchar(15) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;


CREATE TABLE `servers` (
  `server_id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(128) NOT NULL DEFAULT '',
  `ipAddress` varchar(128) NOT NULL DEFAULT '',
  `notes` varchar(512) DEFAULT NULL,
  PRIMARY KEY (`server_id`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8;


CREATE TABLE `users` (
  `username` varchar(45) NOT NULL,
  `password` varchar(128) NOT NULL,
  `enabled` tinyint(4) NOT NULL DEFAULT '1',
  PRIMARY KEY (`username`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


CREATE TABLE `user_roles` (
  `user_role_id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(45) NOT NULL,
  `role` varchar(45) NOT NULL,
  PRIMARY KEY (`user_role_id`),
  UNIQUE KEY `uni_username_role` (`role`,`username`),
  KEY `fk_username_idx` (`username`),
  CONSTRAINT `fk_username` FOREIGN KEY (`username`) REFERENCES `users` (`username`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;




SET @PREVIOUS_FOREIGN_KEY_CHECKS = @@FOREIGN_KEY_CHECKS;
SET FOREIGN_KEY_CHECKS = 0;


LOCK TABLES `CONTACTS` WRITE;
ALTER TABLE `CONTACTS` DISABLE KEYS;
INSERT INTO `CONTACTS` (`id`, `name`, `address`, `gender`, `dob`, `email`, `mobile`, `phone`) VALUES 
  (1,'Sven Bjorkensen','6201 Bjorkensen Ranch Rd.','M','1974-01-01 00:00:00','sven@svennet.net','333-555-1212',NULL),
  (3,'Granny Smith','The Grave','F','1881-02-17 00:00:00','gsmith@iamdead.com','',NULL),
  (4,'Jemma Jones','6023 Somewhere street','F',NULL,'jemma@drjones.com','333-555-1212',NULL);
ALTER TABLE `CONTACTS` ENABLE KEYS;
UNLOCK TABLES;


LOCK TABLES `servers` WRITE;
ALTER TABLE `servers` DISABLE KEYS;
INSERT INTO `servers` (`server_id`, `name`, `ipAddress`, `notes`) VALUES 
  (1,'server1.example.com','10.1.2.13','This really is a server'),
  (2,'server2.example.com','10.1.2.3','This is also a server'),
  (3,'thisserver.example.com','10.20.30.40',NULL),
  (4,'thatserver.example.com','192.168.20.30','Lives in the DMZ'),
  (5,'nearserver.example.com','192.168.10.52','Partner access server'),
  (6,'farserver.example.com','192.168.216.67','Hosted at partner site.');
ALTER TABLE `servers` ENABLE KEYS;
UNLOCK TABLES;


LOCK TABLES `users` WRITE;
ALTER TABLE `users` DISABLE KEYS;
INSERT INTO `users` (`username`, `password`, `enabled`) VALUES 
  ('guest','$2a$10$ps50I69iXeH8hzXEWCs1H.yJgi13uST11Ofl5c568rGwl3C.A/kuW',1),
  ('sporkboy','$2a$10$.j0Iuur.2Rp6emxEqxwwbeGJhbvtCCQN4BEbs1kClYzF9dRWiPiPi',1);
ALTER TABLE `users` ENABLE KEYS;
UNLOCK TABLES;


LOCK TABLES `user_roles` WRITE;
ALTER TABLE `user_roles` DISABLE KEYS;
INSERT INTO `user_roles` (`user_role_id`, `username`, `role`) VALUES 
  (2,'sporkboy','ROLE_ADMIN'),
  (3,'guest','ROLE_USER'),
  (1,'sporkboy','ROLE_USER');
ALTER TABLE `user_roles` ENABLE KEYS;
UNLOCK TABLES;




SET FOREIGN_KEY_CHECKS = @PREVIOUS_FOREIGN_KEY_CHECKS;