package com.theuberlab.smvctiles.tools;

import org.slf4j.LoggerFactory;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

/**
 * PasswordEncoderGenerator Used to generate encrypted values to stuff into a database.
 * 
 * Needs to be modified so that it takes arguments. 
 * 
 * @author Aaron Forster 
 * @date Jan 29, 2015
 */
public class PasswordEncoderGenerator {
	/*
	 * ###########################################################
	 *    Attributes
	 * ###########################################################
	 */
	private final static org.slf4j.Logger logger = LoggerFactory
			.getLogger(PasswordEncoderGenerator.class);
	
	protected static String unHashedPass = "P@ssth1ng";
	
	//TODO: make this no longer static
	private static String getHashedPassword(String unhashed) {
		BCryptPasswordEncoder passwordEncoder = new BCryptPasswordEncoder();
		return passwordEncoder.encode(unhashed);
		
	}
	
	public static void main(String[] args) {
//		String password = "guest";
		
		String hashedPassword = getHashedPassword(unHashedPass);

		System.out.println("The hashed value of [" + unHashedPass + "] is [" + hashedPassword + "].");
	}
}
