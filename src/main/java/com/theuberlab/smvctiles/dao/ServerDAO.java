package com.theuberlab.smvctiles.dao;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.hibernate.sql.JoinType;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.theuberlab.smvctiles.objects.Server;

/**
 * ServerDAO 
 * 
 * @author Aaron Forster 
 * @date Feb 5, 2015
 */
@Repository
@Transactional
public class ServerDAO {
	/*
	 * ########################################################################
	 * Attributes
	 * ########################################################################
	 */
	private final static org.slf4j.Logger logger = LoggerFactory
			.getLogger(ServerDAO.class);
	/*
	 * ########################################################################
	 * Attributes
	 * ########################################################################
	 */

	@Autowired
	private SessionFactory sessionFactory;


	public Server getServerById(int id) {
		return (Server) sessionFactory.getCurrentSession().get(Server.class,
				id);
	}
	
	public List searchServers(String name) {
		Criteria criteria = sessionFactory.getCurrentSession().createCriteria(
				Server.class);
		criteria.add(Restrictions.ilike("name", name + "%"));
		return criteria.list();
	}
	
	public List getAllServers() {
		Criteria criteria = sessionFactory.getCurrentSession().createCriteria(
				Server.class);
		// Without this hibernate will return each datacenter the number of times that it has servers.
		criteria.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY);
		
		return criteria.list();
	}
	
	/**
	 * Gets all servers for the specified DataCenter.
	 * 
	 * @param datacententerid
	 * @return
	 */
	public List getAllServers(int datacententerid) {
//		DetachedCriteria criteria = DetachedCriteria.forClass(Server.class);
		Criteria criteria = sessionFactory.getCurrentSession().createCriteria(Server.class);
		
		
		
		
		
		// This is a sql query that gets results like I want.
//		SELECT servers.name, serverdatacentermapping.datacenter_id
//		FROM servers
//		RIGHT OUTER JOIN serverdatacentermapping
//		ON servers.server_id=serverdatacentermapping.server_id
//		where datacenter_id=2
		

		
		
//		// Create a Hibernate query (HQL)
//		Query query = session.createQuery("FROM servers as p WHERE p.id="+personId);
//		   
//		
//		Person person = (Person) query.uniqueResult();
		
		Query query = sessionFactory.getCurrentSession().createQuery("FROM servers s "
				+ "LEFT JOIN serverdatacentermapping "
				+ "ON s.server_id=serverdatacentermapping.server_id "
				+ "where serverdatacentermapping.datacenter_id=" + datacententerid);
		
		
		
		List<Server> servers = query.list();
		
		//SQL query which works.
//		SELECT servers.name
//		FROM servers
//		LEFT JOIN serverdatacentermapping
//		ON servers.server_id=serverdatacentermapping.server_id
//		where serverdatacentermapping.datacenter_id=2
		
		
		
//		criteria.createCriteria("datacenter", "datacenters")
//			.add(Restrictions.eq("id", datacententerid));
		
//		Criteria criteria = sessionFactory.getCurrentSession().createCriteria(
//				Server.class);
//		return criteria.list();
		
		return query.list();
	}

	public int save(Server server) {
		return (Integer) sessionFactory.getCurrentSession().save(server);
	}

	public void update(Server server) {
		sessionFactory.getCurrentSession().merge(server);
	}

	public void delete(int id) {
		Server server = getServerById(id);
		sessionFactory.getCurrentSession().delete(server);
	}
}
