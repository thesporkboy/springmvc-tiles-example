package com.theuberlab.smvctiles.dao;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.theuberlab.smvctiles.objects.DataCenter;

/**
 * DataCenterDAO 
 * 
 * @author Aaron Forster 
 * @date Feb 6, 2015
 */
@Repository
@Transactional
public class DataCenterDAO {
	/*
	 * ########################################################################
	 * Attributes
	 * ########################################################################
	 */
	private final static org.slf4j.Logger logger = LoggerFactory
			.getLogger(DataCenterDAO.class);

	
	@Autowired
	private SessionFactory sessionFactory;


	public DataCenter getDataCenterById(int id) {
		return (DataCenter) sessionFactory.getCurrentSession().get(DataCenter.class,
				id);
	}
	
	public List<DataCenter> searchDataCenters(String name) {
		Criteria criteria = sessionFactory.getCurrentSession().createCriteria(
				DataCenter.class);
		criteria.add(Restrictions.ilike("name", name + "%"));
		return criteria.list();
	}
	
	public List<DataCenter> getAllDataCenters() {
		Criteria criteria = sessionFactory.getCurrentSession().createCriteria(
				DataCenter.class);
		
		// Without this hibernate will return each datacenter the number of times that it has servers.
		criteria.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY);
		
		List<DataCenter> results = criteria.list();
		
		logger.debug("Returning [{}] results", results.size());
		return results;
//		return criteria.list();
	}
	
	public int save(DataCenter datacenter) {
		return (Integer) sessionFactory.getCurrentSession().save(datacenter);
	}

	public void update(DataCenter datacenter) {
		sessionFactory.getCurrentSession().merge(datacenter);
	}
	
	public void delete(int id) {
		DataCenter datacenter = getDataCenterById(id);
		sessionFactory.getCurrentSession().delete(datacenter);
	}
}
