package com.theuberlab.smvctiles.validators;

import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

import com.theuberlab.smvctiles.objects.Server;

/**
 * ServerFormValidator 
 * 
 * @author Aaron Forster 
 * @date Feb 5, 2015
 */
@Component("datacenterFormValidator")
public class DataCenterFormValidator implements Validator {
	private final static org.slf4j.Logger logger = LoggerFactory
			.getLogger(DataCenterFormValidator.class);

	@Override
	public boolean supports(Class clazz) {
		return Server.class.isAssignableFrom(clazz);
	}

	@Override
	public void validate(Object model, Errors errors) {
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "name",
				"required.name", "Name is required.");
	}
}
