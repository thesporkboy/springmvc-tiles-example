package com.theuberlab.smvctiles.objects;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.apache.commons.lang.builder.ToStringBuilder;
import org.slf4j.LoggerFactory;

/**
 * Server 
 * 
 * @author Aaron Forster 
 * @date Feb 5, 2015
 */
@Entity
@Table(name = "servers")
public class Server {
	/*
	 * ########################################################################
	 * Attributes
	 * ########################################################################
	 */
	private final static org.slf4j.Logger logger = LoggerFactory
			.getLogger(Server.class);
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private int server_id;
	@Column
	private String name;
	@Column
	private String ipAddress;
	@Column
	private String notes;
	
//	@ManyToOne
//    @JoinColumn(name="datacenter")
//    private DataCenter datacenter;
//	
//	@Column
//	private int datacenter;
	//TODO: add a "last polled" date field.
//	@Column
//	private Date dob;
	/*
	 * ########################################################################
	 * Constructors
	 * ########################################################################
	 */

	/*
	 * ########################################################################
	 * Methods
	 * ########################################################################
	 */
	/*
	 * ########################################################################
	 * Methods
	 * ########################################################################
	 */
	
	@Override
	public String toString() {
		return ToStringBuilder.reflectionToString(this);
	}
	
	/*
	 * ########################################################################
	 * Getters and Setters
	 * ########################################################################
	 */
	
	/**
	 * @return the server_id
	 */
	public int getServer_id() {
		return server_id;
	}

	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @return the ipAddress
	 */
	public String getIpAddress() {
		return ipAddress;
	}

	/**
	 * @return the notes
	 */
	public String getNotes() {
		return notes;
	}
	
//	/**
//	 * @return the datacenter
//	 */
//	public DataCenter getDatacenter() {
//		return datacenter;
//	}
	
	/**
	 * @param id the id to set
	 */
	public void setServer_id(int server_id) {
		this.server_id = server_id;
	}

	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * @param address the address to set
	 */
	public void setIpAddress(String ipAddress) {
		this.ipAddress = ipAddress;
	}

	/**
	 * @param notes the notes to set
	 */
	public void setNotes(String notes) {
		this.notes = notes;
	}

//	/**
//	 * @param datacenter the datacenter to set
//	 */
//	public void setDatacenter(DataCenter datacenter) {
//		this.datacenter = datacenter;
//	}
	
	
}
