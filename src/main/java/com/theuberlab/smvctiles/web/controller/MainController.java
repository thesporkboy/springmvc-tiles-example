package com.theuberlab.smvctiles.web.controller;

import org.slf4j.LoggerFactory;
import org.springframework.security.authentication.AnonymousAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;

/**
 * MainController handles requests for default objects such as the index page or the about page. 
 * 
 * @author Aaron Forster 
 * @date Feb 13, 2015
 */
@Controller
public class MainController extends WebMvcConfigurerAdapter {
	private final static org.slf4j.Logger logger = LoggerFactory.getLogger(MainController.class);
	protected String applicationTitle = "Spring MVC - Apache Tiles";
	protected String applicationSubTitle = "A Full Featured Spring MVC and Apache Tiles Example.";
	protected String applicationMessage;

	//TODO: figure out how to do this @RequestMapping(value = "/{name}", method = RequestMethod.GET) in a way that includes /
	/**
	 * Responds to requests for root and/or for the index tile.
	 * RequestMapping's for which the value element does not begin with a leading /
	 * are from the newer tiles based config. Those which begin with a / are likely 
	 * traditional requests which need to be transitioned.
	 * 
	 * @return
	 */
	@RequestMapping(value = { "/", "index" }, method = RequestMethod.GET)
	public ModelAndView index() {
		logger.debug("Request for index.");
		String toolTitle = "Welcome";
		String viewName = "index";
		String viewTitle = applicationTitle + " - " + toolTitle;
		String viewMessage = "By: Aaron Forster";
		
		
		ModelAndView model = new ModelAndView();
		// Set currentSection to index since they just requested the index.
		model.addObject("currentSection", viewName);
		model.addObject("title", viewTitle);
		model.addObject("subtitle", applicationSubTitle);
		model.addObject("message", viewMessage);
		model.setViewName(viewName);
		
//		response.addCookie(new Cookie("foo", "bar"));
		
		return model;
	}
	
	/**
	 * The about page.
	 * 
	 */
	@RequestMapping(value = "about", method = RequestMethod.GET)
	public ModelAndView aboutPage() {
		logger.debug("MainController: request for about.");
		
		//This could easily be populated from a database lookup.
		String aboutTileBody = "<p>An example of Spring MVC with Apache Tiles using a database for back-end authentication and built with Maven and Eclipse.</p>"
				+ "<p>Critical portions of the code are based originally on several other bodies of work:</p>"
				+ "<p><a href=\"http://www.mkyong.com/spring-security/spring-security-form-login-using-database/\" target=\"_blank\">Spring Security Form Login Using Database - XML and Annotation</a> example from mkyong. To get spring3 mvc, database authentication in an eclipse maven project."
				+ "<p>And <a href=\"http://www.techzoo.org/spring-framework/spring-mvc-tiles-3-integration-tutorial.html\" target=\"_blank\">the Spring MVC + Apache Tiles</a> example from Tousif Khan  which helped me get the tiles components working.</p>"
				+ "<p>I integrated functionality from the <a href=\"http://www.javacodegeeks.com/2011/04/spring-mvc3-hibernate-crud-sample.html\">Spring MVC3 Hibernate CRUD Sample Application</a> in order to figure out how to submit forms and modify database content with hibernate. Though the example is somewhat sketchy due to formatting. I spent a great deal of time troubleshooting JSP issues before finally getting it to work. Once done the hibernate/mysql functionality worked flawlessly.</p>"
				+ "<p>The <a href=\"http://krams915.blogspot.com/2011/03/spring-hibernate-one-to-many_20.html\">Spring Hibernate One to Many</a> tutorial by Mark Serrano helped me figure out how to deal with displaying and updating data which required table joins.</p>"
				+ "<p>The project <a href=\"https://bitbucket.org/omadawn/springmvc-tiles-example/wiki/Home\" target=\"_blank\">can be found on github</a> and should eventually produce a tutorial and a maven archetype.</p>";
		
		ModelAndView model = new ModelAndView();
		model.addObject("currentSection", "about");
		model.addObject("title", applicationTitle + " - About");
		model.addObject("subtitle", applicationSubTitle);
//		model.addObject("message", "A Full Featured Spring MVC and Apache Tiles Example.");
		model.addObject("tileBody", aboutTileBody);
		model.setViewName("about");

		return model;
	}
	
	/**
	 * Not really in use yet but will manage requests for the login page.
	 * 
	 * @return
	 */
	@RequestMapping(value = "login", method = RequestMethod.GET)
	public ModelAndView login(@RequestParam(value = "error", required = false) String error,
			@RequestParam(value = "logout", required = false) String logout) {
		logger.debug("Request for login.");
		
		ModelAndView model = new ModelAndView();
		model.addObject("title","Please sign in");
		model.addObject("message", "Login with guest:P@ssth1ng or sporkboy:P@ssw0rd to see different results.");
		
		if (error != null) {
			model.addObject("error", "Invalid username and password!");
		}

		if (logout != null) {
			model.addObject("msg", "You've been logged out successfully.");
		}
		model.setViewName("login");

		return model;

	}	
	
	//TODO: Decide if I still want this.
	
	/**
	 * Returns a custom access denied (403) page.
	 * 
	 * @return
	 */
	@RequestMapping("/403")
	public ModelAndView accesssDenied() {
		logger.debug("MainController: request for 403.");

		ModelAndView model = new ModelAndView();
		
		// Check if user is logged in.
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		if (!(auth instanceof AnonymousAuthenticationToken)) {
			UserDetails userDetail = (UserDetails) auth.getPrincipal();
			System.out.println(userDetail);
		
			model.addObject("username", userDetail.getUsername());
			
		}
		
		model.setViewName("403");
		return model;
	}
}