package com.theuberlab.smvctiles.web.controller;

import java.util.List;

import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.support.PropertySourcesPlaceholderConfigurer;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.support.SessionStatus;
import org.springframework.web.servlet.ModelAndView;

import com.theuberlab.smvctiles.dao.DataCenterDAO;
import com.theuberlab.smvctiles.dao.ServerDAO;
import com.theuberlab.smvctiles.objects.DataCenter;
import com.theuberlab.smvctiles.validators.DataCenterFormValidator;

/**
 * DataCenterController is responsible for handling requests related to Data Centers.
 * This will ultimately end up being merged with DataCenterController. 
 * 
 * @author Aaron Forster 
 * @date Feb 5, 2015
 */
@Controller
@RequestMapping("/datacenter")
public class DataCenterController {
	/*
	 * ########################################################################
	 * Attributes
	 * ########################################################################
	 */
	private final static org.slf4j.Logger logger = LoggerFactory
			.getLogger(DataCenterController.class);
	protected String applicationTitle = "Spring MVC - Apache Tiles";
	protected String applicationSubTitle = "A Full Featured Spring MVC and Apache Tiles Example.";
	protected String applicationMessage;
	protected String moduleName = "Data Center";
	
	/**
	 * The DAO object for datacenters.
	 */
	@Autowired
	private DataCenterDAO datacenterDAO;
	
	/**
	 * The DAO object for servers. 
	 * This might not be neccesary anymore.
	 */
	@Autowired
	private ServerDAO serverDAO;

	/**
	 * The form validator for the datacenter form.
	 */
	@Autowired
	private DataCenterFormValidator validator;

	/*
	 * ########################################################################
	 * Methods
	 * ########################################################################
	 */
	
	/*
	 * This might not be neccesary. 
	 */
	@Bean
	public static PropertySourcesPlaceholderConfigurer propertySourcesPlaceholderConfigurer() {
	    return new PropertySourcesPlaceholderConfigurer();
	}
	
	/*
	 * I may need something like this later. This has something to do with validating data returned from the browser.
	 */
//	@InitBinder
//	public void initBinder(WebDataBinder binder) {
//		SimpleDateFormat dateFormat = new SimpleDateFormat("MM-dd-yyyy");
//		dateFormat.setLenient(false);
//		binder.registerCustomEditor(Date.class, new CustomDateEditor(
//				dateFormat, true));
//	}
	
	/**
	 * The base request for the DataCenter module. Shows all Data Centers.
	 * 
	 * @return
	 */
	@RequestMapping("viewAll")
	public ModelAndView getAllDatacenters() {
		logger.debug("Request for viewAll");
		String viewName = "viewAll";
		String viewTitle = "";
		String viewMessage = "";
		
		ModelAndView mav = new ModelAndView("showDatacenters");
		
		List datacenters = datacenterDAO.getAllDataCenters();
		
		mav.addObject("SEARCH_DATACENTERS_RESULTS_KEY", datacenters);
		
		mav.addObject("currentSection", moduleName);
//		mav.addObject("title", viewTitle);
//		mav.addObject("subtitle", applicationSubTitle);
//		mav.addObject("message", viewMessage);
		
		
		return mav;
	}
	
	/**
	 * Handles requests for the search function.
	 * 
	 * @param name
	 * @return
	 */
	@RequestMapping("search")
	public ModelAndView searchDatacenters(
			@RequestParam(required = true, defaultValue = "") String name) {
		logger.debug("Request for search");
		String viewName = "Search Results";
		
		ModelAndView mav = new ModelAndView("showDatacenters");
		
		List datacenters = datacenterDAO.searchDataCenters(name.trim());
		
		mav.addObject("SEARCH_DATACENTERS_RESULTS_KEY", datacenters);
		
		mav.addObject("currentSection", moduleName);
		mav.addObject("title", viewName);
		
		return mav;
	}
	
	/**
	 * Get requests to save. Called when someone clicks new datacenter.
	 * 
	 * @return
	 */
	@RequestMapping(value = "save", method = RequestMethod.GET)
	public ModelAndView newDataCenterForm() {
		logger.debug("Request for save GET");
		String viewName = "New Data Center";
		
		ModelAndView mav = new ModelAndView("newDatacenter");
		DataCenter datacenter = new DataCenter();
		
		mav.getModelMap().put("newDatacenter", datacenter);
		
		mav.addObject("currentSection", moduleName);
		mav.addObject("title", viewName);
		
		return mav;
		
	}
	
	/**
	 * Post requests to save. Called when someone fills out the new datacenter form and hits submit.
	 * 
	 * @param datacenter
	 * @param resultDatacenter
	 * @param status
	 * @return
	 */
	@RequestMapping(value = "save", method = RequestMethod.POST)
	public String create(@ModelAttribute("newDatacenter") DataCenter datacenter, BindingResult resultDatacenter, 
			SessionStatus status) {
		logger.debug("Request for save POST");
		
		validator.validate(datacenter, resultDatacenter);
		
		if (resultDatacenter.hasErrors()) {
			return "newDatacenter";
		}
		
		datacenterDAO.save(datacenter);
		status.setComplete();
		return "redirect:viewAll";
	}
	
	/**
	 * Get requests for update. Called when a user clicks the edit datacenter option.
	 * 
	 * @param id
	 * @return
	 */
	@RequestMapping(value = "update", method = RequestMethod.GET)
	public ModelAndView edit(@RequestParam Integer id) {
		logger.debug("Request for update GET");
		String viewName = "Edit Data Center";
		
		ModelAndView mav = new ModelAndView("editDatacenter");
		
		DataCenter datacenter = datacenterDAO.getDataCenterById(id);
		
		mav.addObject("editDatacenter", datacenter);
		
		mav.addObject("currentSection", moduleName);
		mav.addObject("title", viewName);
		
		return mav;
	}
	
	/**
	 * Post requests to update. Called once someone has clicked edit on a datacenter object, made their changes and hit submit.
	 * 
	 * @param datacenter
	 * @param result
	 * @param status
	 * @return
	 */
	@RequestMapping(value = "update", method = RequestMethod.POST)
	public String update(@ModelAttribute("editDatacenter") DataCenter datacenter,
			BindingResult result, SessionStatus status) {
		logger.debug("Request for update POST");
		
		validator.validate(datacenter, result);
		if (result.hasErrors()) {
			return "editDatacenter";
		}
		
		datacenterDAO.update(datacenter);
		
		status.setComplete();
		
		return "redirect:viewAll";
	}
	
	/**
	 * Delete datacenter. Called when someone clicks the delete link next do a datacenter.
	 * 
	 * @param id
	 * @return
	 */
	@RequestMapping("delete")
	public ModelAndView delete(@RequestParam("id") Integer id) {
		logger.debug("Request for delete");
		ModelAndView mav = new ModelAndView("redirect:viewAll");
		
		datacenterDAO.delete(id);
		
		return mav;
	}
	
	/*
	 * ########################################################################
	 * Getters and Setters
	 * ########################################################################
	 */
}