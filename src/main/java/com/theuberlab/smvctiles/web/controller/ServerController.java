package com.theuberlab.smvctiles.web.controller;

import java.util.List;

import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.support.PropertySourcesPlaceholderConfigurer;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.support.SessionStatus;
import org.springframework.web.servlet.ModelAndView;

import com.theuberlab.smvctiles.dao.ServerDAO;
import com.theuberlab.smvctiles.objects.Server;
import com.theuberlab.smvctiles.validators.ServerFormValidator;

/**
 * ServerController is responsible for handling CRUD requests related to servers. 
 * 
 * @author Aaron Forster 
 * @date Feb 5, 2015
 */
@Controller
@RequestMapping("/server")
public class ServerController {
	/*
	 * ########################################################################
	 * Attributes
	 * ########################################################################
	 */
	private final static org.slf4j.Logger logger = LoggerFactory
			.getLogger(ServerController.class);
	
	
//	@Controller
//	@RequestMapping("/datacenter")
//	public class DataCenterController {
//
//	@RequestMapping("/search")
//	       public ModelAndView searchDatacenters(stuff) {
//	            ...
//	      }
//	}
	
	/**
	 * The Data Access object for Servers.
	 */
	@Autowired
	private ServerDAO serverDAO;
	
	/**
	 * The form validator for Server objects.
	 */
	@Autowired
	private ServerFormValidator validator;
	
	/*
	 * ########################################################################
	 * Methods
	 * ########################################################################
	 */
	
//	/*
//	 * This might not be neccesary. 
//	 */
//	@Bean
//	public static PropertySourcesPlaceholderConfigurer propertySourcesPlaceholderConfigurer() {
//	    return new PropertySourcesPlaceholderConfigurer();
//	}
	
	/*
	 * I may need something like this later.
	 */
//	@InitBinder
//	public void initBinder(WebDataBinder binder) {
//		SimpleDateFormat dateFormat = new SimpleDateFormat("MM-dd-yyyy");
//		dateFormat.setLenient(false);
//		binder.registerCustomEditor(Date.class, new CustomDateEditor(
//				dateFormat, true));
//	}



//	@RequestMapping("/viewAllServers")
	@RequestMapping("viewAll")
	public ModelAndView getAllServers() {
		logger.debug("Request for viewAll");
		
		ModelAndView mav = new ModelAndView("showServers");
		
		List servers = serverDAO.getAllServers();
		
		mav.addObject("SEARCH_SERVERS_RESULTS_KEY", servers);
		
		return mav;
	}
	
//	@RequestMapping("/searchServers")
	@RequestMapping("search")
	public ModelAndView searchServers(
			@RequestParam(required = false, defaultValue = "") String name) {
		logger.debug("Request for search");
		ModelAndView mav = new ModelAndView("showServers");
		List servers = serverDAO.searchServers(name.trim());
		mav.addObject("SEARCH_SERVERS_RESULTS_KEY", servers);
		return mav;
	}
	
//	@RequestMapping(value = "/saveServer", method = RequestMethod.GET)
	@RequestMapping(value = "save", method = RequestMethod.GET)
	public ModelAndView newServerForm() {
		logger.debug("Request for save GET");
		ModelAndView mav = new ModelAndView("newServer");
		Server server = new Server();
		
		mav.getModelMap().put("newServer", server);
		return mav;
	}
	
//	@RequestMapping(value = "/saveServer", method = RequestMethod.POST)
	@RequestMapping(value = "save", method = RequestMethod.POST)
	public String create(@ModelAttribute("newServer") Server server,
			BindingResult result, SessionStatus status) {
		logger.debug("Request for save POST");
		validator.validate(server, result);
		if (result.hasErrors()) {
			return "newServer";
		}
		serverDAO.save(server);
		status.setComplete();
		return "redirect:viewAll";
	}
	
//	@RequestMapping(value = "/updateServer", method = RequestMethod.GET)
	@RequestMapping(value = "update", method = RequestMethod.GET)
	public ModelAndView edit(@RequestParam Integer server_id) {
		logger.debug("Request for update GET");
		ModelAndView mav = new ModelAndView("editServer");
		Server server = serverDAO.getServerById(server_id);
		mav.addObject("editServer", server);
		return mav;
	}
	
//	@RequestMapping(value = "/updateServer", method = RequestMethod.POST)
	@RequestMapping(value = "update", method = RequestMethod.POST)
	public String update(@ModelAttribute("editServer") Server server,
			BindingResult result, SessionStatus status) {
		logger.debug("Request for update POST");
		validator.validate(server, result);
		if (result.hasErrors()) {
			return "editServer";
		}
		serverDAO.update(server);
		status.setComplete();
		return "redirect:viewAll";
	}
	
//	@RequestMapping("deleteServer")
	@RequestMapping("delete")
	public ModelAndView delete(@RequestParam("server_id") Integer server_id) {
		logger.debug("Request for delete");
		// Might need to rediret to /server/viewAll or server/viewAll or something
		ModelAndView mav = new ModelAndView("redirect:viewAll");
		serverDAO.delete(server_id);
		return mav;
	}
}
