// General functions for the server and datacenter modules. These should be made more generic.
function go(url) {
	window.location = url;
}
function deleteServer(url) {
	var confirmDelete = confirm("Are you sure you want to delete this server?");
	if (confirmDelete) {
		go(url);
	}
}
function deleteDatacenter(url) {
	var confirmDelete = confirm("Are you sure you want to delete this Data Center?");
	if (confirmDelete) {
		go(url);
	}
}

// This code is used to add and remove new rows when adding or modifying a data center.
function getElementsByClassName(c,el){
    if(typeof el=='string'){el=document.getElementById(el);}
    if(!el){el=document;}
    if(el.getElementsByClassName){return el.getElementsByClassName(c);}
    var arr=[],
        allEls=el.getElementsByTagName('*');
    for(var i=0;i<allEls.length;i++){
        if(allEls[i].className.split(' ').indexOf(c)>-1){arr.push(allEls[i]);}
    }
    return arr;
}
function killMe(el){
	console.log("Attempting to remove child.");
    return el.parentNode.removeChild(el);
}
function getParentByTagName(el,tag){
    tag=tag.toLowerCase();
    while(el.nodeName.toLowerCase()!=tag){
        el=el.parentNode;
    }
    return el;
}
function delRow(){
	console.log("Deleting table row.");
    killMe(getParentByTagName(this,'tr'));
}
function addRow() {
    var table = getParentByTagName(this,'table');
    console.log("table length [" + table.rows.length + "]");
    var lastInputs=table.rows.length>2?
        table.rows[table.rows.length-2].getElementsByTagName('input'):[];
    for(var i=0;i<lastInputs.length-1;i++){
        if(lastInputs[i].value==''){
//        	This is left as an example of how to do some simple validation. In practice however I would not want to restrict the user from creating multiple rows if they new they were going to enter multiple rows. This sort of validation should be done on form submit and empty rows should be deleted.
        	window.alert("Please fill out the previous item before adding a new one.");
        	return false;}
    }
    var rowCount = table.rows.length;
    //TODO: This is a crap way to derive the current server. I REALLY should be tagging each input row somehow and only counting those. Instead I am counting all table rows. Currently if you modify the form to add say another header row or rows in between each server this will need to be updated. Note the latter will make the 'edit' and 'new' functions require separate JavaScript.
    var currentServer = rowCount - 3;
    var rowPath = "servers[" + currentServer + "]";
    var row = table.insertRow(rowCount-1);
    console.log("Adding row [" + rowCount + "] server [" + currentServer + "]");
    
    var cell1 = row.insertCell(0);
    var element1 = document.createElement("input");
    element1.type = "text";
    element1.id = rowPath + ".name";
    element1.name = rowPath + ".name";
    cell1.appendChild(element1);

    var cell2 = row.insertCell(1);
    var element2 = document.createElement("input");
    element2.type = "text";
    element2.id = rowPath + ".ipAddress";
    element2.name = rowPath + ".ipAddress";
    cell2.appendChild(element2);
    
    var cell3 = row.insertCell(2);
    var element3 = document.createElement("input");
    element3.type = "button";
    element3.className="del";
    element3.value='-';
    element3.onclick=delRow;
    cell3.appendChild(element3);
}

//Trying to do the collapsable lists thing. This is one of the ways I've seen to do it. Not currently working though.
$(".aro > .wrap").click(function () {
    if (!$(this).parent().hasClass("active")) {
        $(".active .details").slideUp("slow");
        $(".aro").removeClass("active");
        $(this).parent().addClass("active");
        $(this).parent().children(".details").slideDown("slow", function() {
            $('html, body').animate({
                scrollTop: $(this).parent().offset().top
            }, 2000);
        });
    } else {
        $(this).parent().removeClass("active");
        $(this).parent().children(".details").slideUp("slow");
    }
});