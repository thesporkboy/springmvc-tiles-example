<%@include file="taglib_includes.jsp" %>
<div style="margin:10px;">
	<c:if test="${! empty title}">
		<h3>${title}</h3>
	</c:if>
	<c:if test="${! empty subtitle}">
		<h4>${subtitle}</h4>
	</c:if>
	<c:if test="${! empty message}">
		<p>${message}</p>
	</c:if>
	
</div>