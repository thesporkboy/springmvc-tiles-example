<%@include file="../taglib_includes.jsp"%>
<div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
	<c:if test="${! empty title}">
		<h1 class="page-header">${title}</h1>
	</c:if>
	<c:if test="${! empty subtitle}">
		<h2 class="sub-header">${subtitle}</h2>
	</c:if>
	<c:if test="${! empty message}">
		<p>${message}</p>
	</c:if>
    
		<form action="search" method="post">
		<!-- This hidden parameter is used by spring security to provide CSRF protection. If it does not exist in all forms the user will get a 403. -->
		<input type="hidden" name="${_csrf.parameterName}"
			value="${_csrf.token}" />
			
			
			
			
          <div class="table-responsive">
            <table class="table table-striped">
            	<tbody>
					<tr>
						<td>Search</td>
						<td><input name="name" type="text"> <input
							value="Search" type="submit"> <input value="New Datacenter"
							onclick="javascript:go('save');" type="button">

						</td>
					</tr>
				</tbody>
			</table>
			</div>
		</form>
		
		<div class="table-responsive">
            <table class="table table-striped">
              <thead>
                <tr>
                  <th>Name</th>
                  <th>Location</th>
                </tr>
              </thead>
              <tbody>
				<c:if test="${empty SEARCH_DATACENTERS_RESULTS_KEY}">
					<tr>
						<td colspan="4">No Datacenters found which match your search</td>
					</tr>
				</c:if>
				<c:if test="${! empty SEARCH_DATACENTERS_RESULTS_KEY}">
					<c:forEach var="datacenter" items="${SEARCH_DATACENTERS_RESULTS_KEY}">
						<tr>
							<td><c:out value="${datacenter.name}"></c:out></td>
							<td><c:out value="${datacenter.location}"></c:out></td>
							<td>
								<a href="updateDatacenter?id=${datacenter.id}">Edit</a>
								<a href="javascript:deleteDatacenter('delete?id=${datacenter.id}');">Delete</a>
							</td>
							<c:if test="${!empty datacenter.servers}">
								<tr>
								<td></td>
 								<td><c:forEach var="server" items="${datacenter.servers}">
										<p>${server.name}</p>
									</c:forEach>
								</td>
								</tr>
							</c:if>
						</tr>
					</c:forEach>
				</c:if>
			</tbody>
		</table>
	</div>
</div>