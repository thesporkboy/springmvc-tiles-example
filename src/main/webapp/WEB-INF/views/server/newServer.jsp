<%@include file="../taglib_includes.jsp"%>
<div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">

	<c:if test="${! empty title}">
		<h1 class="page-header">${title}</h1>
	</c:if>
	<c:if test="${! empty subtitle}">
		<h2 class="sub-header">${subtitle}</h2>
	</c:if>
	<c:if test="${! empty message}">
		<p>${message}</p>
	</c:if>
		
		<div class="table-responsive">
            <table class="table table-striped">
        
        <thead>
                <tr>
                  <th>Edit Server</th>
                  </tr>
                  </thead>
	<tbody>
		<tr valign="top" align="center">
			<td align="center">
				<form:form action="save"
					method="post" commandname="newServer" modelAttribute="newServer">
					<!-- If this hidden attribute does not exist in all forms the user will get a 403. -->
					<input type="hidden" name="${_csrf.parameterName}"
						value="${_csrf.token}" />
					<table style="border-collapse: collapse;">
					<!-- <table style="border-collapse: collapse;" width="500" 
						bordercolor="#006699" cellpadding="2" cellspacing="2"> -->
						<tbody>
							<tr>
								<td width="100" align="right">Name</td>
								<td width="150"><form:input path="name"></form:input></td>
								<td align="left"><form:errors path="name"
										cssstyle="color:red"></form:errors></td>
							</tr>
							<tr>
								<td width="100" align="right">Address</td>
								<td><form:input path="ipAddress"></form:input></td>
								<td align="left">
									<form:errors path="ipAddress" cssstyle="color:red"/>
								</td>
							</tr>
							<tr>
								<td width="100" align="right">Notes</td>
								<td><form:input path="notes"></form:input></td>
								<td align="left"><form:errors path="notes"
										cssstyle="color:red"></form:errors></td>
							</tr>
							<tr>
								<td colspan="3" align="center"><input name="" value="Save"
									type="submit"> <input name="" value="Reset"
									type="reset"> <input value="Back"
									onclick="javascript:go('viewAll');" type="button">

								</td>
							</tr>
						</tbody>
					</table>
				</form:form></td>
		</tr>
	</tbody>
</table>
</div>
</div>