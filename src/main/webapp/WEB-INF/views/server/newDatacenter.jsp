<%@include file="../taglib_includes.jsp"%>
<div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
<c:if test="${! empty title}">
		<h1 class="page-header">${title}</h1>
	</c:if>
	<c:if test="${! empty subtitle}">
		<h2 class="sub-header">${subtitle}</h2>
	</c:if>
	<c:if test="${! empty message}">
		<p>${message}</p>
	</c:if>


<!-- <table id="tableId">
    <tr>
        <th>Host Name</th>
        <th>Directory</th>
        <td><input class="add" type="button" value="+" /></td>
    </tr>
    <tr>
        <td></td><td></td>
        <td><input class="add" type="button" value="+" /></td>
    </tr>
</table>
<script type="text/javascript">
    (function(){
    var els=getElementsByClassName("add","tableId");
    for(var i=0;i<els.length;i++){
        els[i].onclick=addRow;
    }
    els[0].onclick();
    })();
</script> -->



<div class="table-responsive">
	<table class="table" id="newDCTable">
              
	<tbody>
		<tr valign="top" align="center">
			<td align="center">
				<form:form action="/datacenter/save"
					method="post" commandname="newDatacenter" modelAttribute="newDatacenter">
					<!-- If this hidden attribute does not exist in all forms the user will get a 403. -->
					<input type="hidden" name="${_csrf.parameterName}"
						value="${_csrf.token}" />
					<table id="newDatacenterTable" style="border-collapse: collapse;">
						<tbody>
							<tr>
								<td width="100" align="right">Name</td>
								<td width="150"><form:input path="name"></form:input></td>
								<td align="left"><form:errors path="name"
										cssstyle="color:red"></form:errors></td>
							</tr>
							<tr>
								<td width="100" align="right">Location</td>
								<td><form:input path="location"></form:input></td>
								<td align="left">
									<form:errors path="location" cssstyle="color:red"/>
								</td>
							</tr>
	<table id="newServers">
	<tr>
		<td width="100" align="right">Servers</td>				
	</tr>
	<tr>
        <th>Host Name</th>
        <th>IP Address</th>
        <td><input class="add" type="button" value="+" /></td>
    </tr>
    <tr>
        <td></td><td></td>
        <td><input class="add" type="button" value="+" /></td>
    </tr>
    </table>
    <%-- TODO: Figure out how to get this red error functionality in there. <form:errors path="servers[0].IpAddress" cssstyle="color:red"/> --%>
    
							<!-- This basically works but it is tricky. -->
<%--  							<tr>
									<td width="100" align="right">Server</td>
									<td width="100" align="right">Name</td>
									<td><form:input path="servers[0].name"></form:input></td>
									<td align="left">
										<form:errors path="servers[0].name" cssstyle="color:red"/>
									</td>
									<td width="100" align="right">Address</td>
									<td><form:input path="servers[0].IpAddress"></form:input></td>
									<td align="left">
										<form:errors path="servers[0].IpAddress" cssstyle="color:red"/>
									</td>
							</tr> --%>
							
							<tr>
								<td colspan="3" align="center"><input name="" value="Save"
									type="submit"> <input name="" value="Reset"
									type="reset"> <input value="Back"
									onclick="javascript:go('viewAll');" type="button">

								</td>
							</tr>
						</tbody>
					</table>
				</form:form></td>
		</tr>
	</tbody>
</table>
<script type="text/javascript">
    (function(){
    var els=getElementsByClassName("add","newServers");
    for(var i=0;i<els.length;i++){
        els[i].onclick=addRow;
    }
    els[0].onclick();
    })();
</script>
</div>
</div>