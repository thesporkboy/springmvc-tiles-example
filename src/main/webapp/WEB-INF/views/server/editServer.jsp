<%@include file="../taglib_includes.jsp"%>
<div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">

	<c:if test="${! empty title}">
		<h1 class="page-header">${title}</h1>
	</c:if>
	<c:if test="${! empty subtitle}">
		<h2 class="sub-header">${subtitle}</h2>
	</c:if>
	<c:if test="${! empty message}">
		<p>${message}</p>
	</c:if>


		<div class="table-responsive">
            <table class="table">
              
	<tbody>
		<tr valign="top" align="center">
			<td align="center">
				<form:form action="update" method="post" commandname="editServer"
					modelAttribute="editServer">
					<!-- If this hidden attribute does not exist in all forms the user will get a 403. -->
					<input type="hidden" name="${_csrf.parameterName}"
						value="${_csrf.token}" />
					<table style="border-collapse: collapse;" >
						<tbody>
							<tr>
								<td width="100" align="right">Name</td>
								<td><form:input path="name"></form:input></td>
								<td align="left"><form:errors path="name"
										cssstyle="color:red"></form:errors></td>
							</tr>
							<tr>
								<td width="100" align="right">Address</td>
								<td><form:input path="ipAddress"></form:input></td>
								<td align="left"><form:errors path="ipAddress"
										cssstyle="color:red"></form:errors></td>
							</tr>
							<tr>
								<td width="100" align="right">Notes</td>
								<td><form:input path="notes"></form:input></td>
								<td align="left"><form:errors path="notes"
										cssstyle="color:red"></form:errors></td>
							</tr>
							<tr valign="bottom">
								<td colspan="3" align="center"><input value="Delete"
									onclick="javascript:deleteServer('delete?server_id=${editServer.server_id}');"
									type="button"> <input name="" value="Save"
									type="submit"> <input value="Back"
									onclick="javascript:go('viewAll');" type="button">

								</td>
							</tr>
						</tbody>
					</table>
					<form:hidden path="server_id" readonly="true"></form:hidden>
				</form:form></td>
		</tr>
	</tbody>
</table>
</div>
</div>