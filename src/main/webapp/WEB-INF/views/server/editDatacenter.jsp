<%@include file="../taglib_includes.jsp"%>
<div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
	<c:if test="${! empty title}">
		<h1 class="page-header">${title}</h1>
	</c:if>
	<c:if test="${! empty subtitle}">
		<h2 class="sub-header">${subtitle}</h2>
	</c:if>
	<c:if test="${! empty message}">
		<p>${message}</p>
	</c:if>

	<div class="table-responsive">
		<table class="table" id="newDCTable">

			<tbody>
				<tr valign="top" align="center">
					<td align="center">
						<%-- modelAttribute is the object being edited and gets passed to the controller class. The controller will convert (hopefully it between
					a javascript object and a java object. In the web browser the object is being modified as a JS object (since your java code is not 
					executing natively in the browser. Then when it gets passed to a pure java method it needs to be converted. The inverse is also true
					when code is rendered for browser consumption. --%> <form:form
							action="update" method="post"
							commandname="editDatacenter" modelAttribute="editDatacenter">
							<!-- If this hidden attribute does not exist in all forms the user will get a 403. -->
							<input type="hidden" name="${_csrf.parameterName}"
								value="${_csrf.token}" />
							<table id="newBigipTable" style="border-collapse: collapse;">
								<tbody>
									<tr>
										<td width="100" align="right">Data Center Name</td>
										<td width="150"><form:input path="name"></form:input></td>
										<td align="left"><form:errors path="name"
												cssstyle="color:red"></form:errors></td>
									</tr>
									<tr>
										<td width="100" align="right">Location</td>
										<td><form:input path="location"></form:input></td>
										<td align="left"><form:errors path="location"
												cssstyle="color:red" /></td>
									</tr>
									<table id="servers">
										<tr>
											<td width="100" align="right">servers</td>
										</tr>
										<tr>
											<th>Host Name</th>
											<th>IP Address</th>
											<td><input class="add" type="button" value="+" /></td>
										</tr>
										<%-- TODO: Figure out how to get this red error functionality in there. <form:errors path="servers[0].IpAddress" cssstyle="color:red"/> --%>
										<%-- Check and see if this datacenter has a servers list defined. --%>
										<c:if test="${!empty editDatacenter.servers}">
											<%-- If so iterate through them and render them. --%>
											<c:forEach var="server" items="${editDatacenter.servers}"
												varStatus="stat">
												<%-- Though we are only using it to get the iteration index the varStatus variable is a complex object that contains much more information. --%>
												<%-- TODO: I have to figure out how to get this to render above each server. Currently it renders above the entire list and even the Data Center fields. --%>
												<%-- <td width="100" align="right">Server</td> --%>
												<tr>
													<%-- Server Name --%>
													<form:hidden path="server_id" readonly="true"></form:hidden>
													<td width="150"><form:input
															path="name"
															placeholder="${server.name}"></form:input></td>
													<td align="left"><form:errors
															path=".name" cssstyle="color:red"
															placeholder="${server.name}"></form:errors></td>

													<%-- Server Address --%>
													<td width="150"><form:input
															path="ipAddress"
															placeholder="${server.ipAddress}"></form:input></td>
													<td align="left"><form:errors
															path="ipAddress"
															cssstyle="color:red" placeholder="${server.ipAddress}"></form:errors></td>
													<td><input class="del" type="button" value="-"
														onclick="javascript:delRow" /></td>
												</tr>
<%-- 																								<tr>
													Server Name
													<form:hidden path="servers[${stat.index}].server_id" id="servers[${stat.index}].server_id" readonly="true"></form:hidden>
													<td width="150"><form:input
															path="servers[${stat.index}].name"
															id="servers[${stat.index}].name"
															placeholder="${server.name}"></form:input></td>
													<td align="left"><form:errors
															path="servers[${stat.index}].name" cssstyle="color:red"
															placeholder="${server.name}"></form:errors></td>

													Server Address
													<td width="150"><form:input
															path="servers[${stat.index}].ipAddress"
															id="servers[${stat.index}].ipAddress"
															placeholder="${server.ipAddress}"></form:input></td>
													<td align="left"><form:errors
															path="servers[${stat.index}].ipAddress"
															cssstyle="color:red" placeholder="${server.ipAddress}"></form:errors></td>
													<td><input class="del" type="button" value="-"
														onclick="javascript:delRow" /></td>
												</tr> --%>
												
												
											</c:forEach>


										</c:if>
										<%-- The DC name and Location fields above use this <form:errors/> syntax. I don't actually understand it well. 
										I do understand that it is how errors like form validation get sent back to the user. Here they are rendered 
										as a second table column next to the normal input field. However this is causing some rendering issues here 
										where I amy trying to render these fields next to one another as the same row instead of separate rows. --%>
										<%-- TODO: figure out how to et this error functioality without adding a space like this does. --%>


										<tr>
											<td></td>
											<td></td>
											<td><input class="add" type="button" value="+" /></td>
										</tr>
									</table>

									<tr>
										<td colspan="3" align="center"><input value="Delete"
											onclick="javascript:deleteDatacenter('delete?id=${editDatacenter.id}');"
											type="button"> <input name="" value="Save"
											type="submit"> <input value="Back"
											onclick="javascript:go('viewAll');" type="button">
										</td>
									</tr>
								</tbody>
							</table>
							<%-- TODO: This seems like a security flaw. --%>
							<form:hidden path="id" readonly="true"></form:hidden>
						</form:form>
					</td>
				</tr>
			</tbody>
		</table>
		<script type="text/javascript">
    (function(){
    var els=getElementsByClassName("add","servers");
    for(var i=0;i<els.length;i++){
        els[i].onclick=addRow;
    }
    var existingEls=getElementsByClassName("del");
    for (var j=0;j<existingEls.length;j++) {
    	existingEls[j].onclick=delRow;
    }
    /* els[0].onclick(); In the 'new' interface we click the new row button once. */
    })();
		</script>
	</div>
</div>