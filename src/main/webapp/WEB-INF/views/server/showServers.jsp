<%@include file="../taglib_includes.jsp"%>
<div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
	<c:if test="${! empty title}">
		<h1 class="page-header">${title}</h1>
	</c:if>
	<c:if test="${! empty subtitle}">
		<h2 class="sub-header">${subtitle}</h2>
	</c:if>
	<c:if test="${! empty message}">
		<p>${message}</p>
	</c:if>

		<form action="search" method="post">
		<!-- This hidden parameter is used by spring security to provide CSRF protection. If it does not exist in all forms the user will get a 403. -->
		<input type="hidden" name="${_csrf.parameterName}"
			value="${_csrf.token}" />
			
			
          <div class="table-responsive">
            <table class="table table-striped">
            	<tbody>
					<tr>
						<td>Search</td>
						<td><input name="name" type="text"> <input
							value="Search" type="submit"> <input value="New Server"
							onclick="javascript:go('save');" type="button">

						</td>
					</tr>
				</tbody>
			</table>
			</div>
			
		</form>
		
		<div class="table-responsive">
            <table class="table table-striped">
              <thead>
                <tr>
                  <th>Name</th>
					<th>IP Address</th>
					<%-- TODO: add this back in. This will require updating the database so that DC actually IS stored in the server record instead of the intermediary table. 
					<th>Data Center</th> --%>
					<th>Notes</th>
                </tr>
              </thead>
              <tbody>
				<c:if test="${empty SEARCH_SERVERS_RESULTS_KEY}">
					<tr>
						<td colspan="4">No Servers found which match your search</td>
					</tr>
				</c:if>
				<c:if test="${! empty SEARCH_SERVERS_RESULTS_KEY}">
					<c:forEach var="server" items="${SEARCH_SERVERS_RESULTS_KEY}">
						<tr>
							<%-- <td><c:out value="${server.server_id}"></c:out></td> --%>
							<td><c:out value="${server.name}"></c:out></td>
							<td><c:out value="${server.ipAddress}"></c:out></td>
							<%-- <td><c:out value="${server.datacenter.name}"></c:out></td> --%>
							<td><c:out value="${server.notes}"></c:out></td>
							<td>
								<a href="update?server_id=${server.server_id}">Edit</a>
								<a href="javascript:deleteServer('delete?server_id=${server.server_id}');">Delete</a>
							</td>
						</tr>
					</c:forEach>
				</c:if>
			</tbody>
		</table>
		</div>
</div>