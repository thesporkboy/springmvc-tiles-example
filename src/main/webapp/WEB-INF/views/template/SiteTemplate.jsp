<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@include file="../taglib_includes.jsp" %>
<%@page session="true"%>
<html lang="en">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
	<title>Spring MVC - Apache Tiles Example</title>

	<%-- <link rel="stylesheet" media="screen" href="${pageContext.request.contextPath}/static/css/main.css"/> --%>
	<%-- Might need to ditch the viewport part. --%>
	<meta name="viewport" content="width=device-width, initial-scale=1.0"/>
	<meta name="author" content="Aaron Forster"/>
	<meta name="description" content="A Full Featured Spring MVC and Apache Tiles Example."/>
	<meta name="robots" content="all"/>


	<%-- Let's try this with bootstrap --%>
	
	<!-- Le styles -->
    <link href="${pageContext.request.contextPath}/static/css/bootstrap.css" rel="stylesheet"/>
    <style type="text/css">
      body {
        padding-top: 60px;
        padding-bottom: 40px;
      }
      .sidebar-nav {
        padding: 9px 0;
      }

      @media (max-width: 980px) {
        /* Enable use of floated navbar text */
        .navbar-text.pull-right {
          float: none;
          padding-left: 5px;
          padding-right: 5px;
        }
      }
    </style>
    <link href="${pageContext.request.contextPath}/static/css/bootstrap-responsive.css" rel="stylesheet"/>

	<!-- For the login page. -->
	<link href="${pageContext.request.contextPath}/static/css/signin.css" rel="stylesheet">

    <!-- HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
      <script src="${pageContext.request.contextPath}/static/js/html5shiv.js"></script>
    <![endif]-->

    <!-- Fav and touch icons -->
    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="${pageContext.request.contextPath}/static/ico/apple-touch-icon-144-precomposed.png"/>
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="${pageContext.request.contextPath}/static/ico/apple-touch-icon-114-precomposed.png"/>
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="${pageContext.request.contextPath}/static/ico/apple-touch-icon-72-precomposed.png"/>
    <link rel="apple-touch-icon-precomposed" href="${pageContext.request.contextPath}/static/ico/apple-touch-icon-57-precomposed.png"/>
    <link rel="shortcut icon" href="${pageContext.request.contextPath}/static/ico/favicon.png"/>
	 
	<%-- TODO: Moved up here temporarily for the new row functionality. Needs to be moved back down to bottom of page for speed. --%>
	<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
	<script type="text/javascript" src="${pageContext.request.contextPath}/static/js/custom.js"></script>
	
	<%-- TODO: Combine these two? --%>	
	<!--[if lt IE 9]>
	<script src="${pageContext.request.contextPath}/static/js/html5shiv.js"></script>
	<![endif]-->

	<!--[if IE]>
	<link rel="stylesheet" href="${pageContext.request.contextPath}/static/css/ie.css" type="text/css" media="screen, projection">
	<![endif]-->
</head>
<body>
	<!-- Page Header Bar -->
	<tiles:insertAttribute name="header" />

	<!-- Everying else lives here. -->
	<div class="container-fluid">
      	<div class="row-fluid">
			<!-- Side Navigation -->
			<tiles:insertAttribute name="menu" />

			<!-- Main Window -->
			<div class="span9">
				<tiles:insertAttribute name="body" />
			</div>
	
		</div>
	</div>	
	
	<!-- Footer -->
	<tiles:insertAttribute name="footer" />
		
    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="${pageContext.request.contextPath}/static/js/bootstrap.min.js"></script>
    <script src="${pageContext.request.contextPath}/static/js/docs.min.js"></script>
    <%-- TODO: Move these from the header back down to here. --%>
    <%-- <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script> --%>
    <%-- <script src="${pageContext.request.contextPath}/static/js/custom.js"></script> --%>
    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
    <script src="${pageContext.request.contextPath}/static/js/ie10-viewport-bug-workaround.js"></script>
	
	<!-- So all my logouts work. -->
	<c:url value="/j_spring_security_logout" var="logoutUrl" />
	<form action="${logoutUrl}" method="post" id="logoutForm">
		<input type="hidden" name="${_csrf.parameterName}"
			value="${_csrf.token}" />
	</form>
	<script>
		function formSubmit() {
			document.getElementById("logoutForm").submit();
		}
	</script>
	
</body>
</html>