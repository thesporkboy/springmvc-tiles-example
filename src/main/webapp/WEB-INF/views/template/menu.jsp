<%@include file="../taglib_includes.jsp"%>
<div class="span3">
	<div class="well sidebar-nav">
		<ul class="nav nav-list">
			<li class="nav-header">General</li>
			<li><spring:url value="about" var="aboutUrl"
						htmlEscape="true" /> <a href="${aboutUrl}">About</a></li>

			<% if ( request.isUserInRole("ROLE_ADMIN") || request.isUserInRole("ROLE_SERVER_ADMIN") || request.isUserInRole("ROLE_DATACENTER_ADMIN") ) { %>
				<li class="nav-header">Administration</li>

				<% if ( request.isUserInRole("ROLE_ADMIN") || request.isUserInRole("ROLE_SERVER_ADMIN") ) { %>
					<li><spring:url value="/server/viewAll" var="serverUrl"
						htmlEscape="true" /> <a href="${serverUrl}">Servers</a></li>
				<% } %>

				<% if ( request.isUserInRole("ROLE_ADMIN") || request.isUserInRole("ROLE_DATACENTER_ADMIN") ) { %>
					<li><spring:url value="/datacenter/viewAll" var="datacenterUrl"
						htmlEscape="true" /> <a href="${datacenterUrl}">Data Centers</a></li>
				<% } %>
				
			<% } %>

		</ul>
	</div><%--/well sidebar-nav --%>
</div><%--/span3--%>