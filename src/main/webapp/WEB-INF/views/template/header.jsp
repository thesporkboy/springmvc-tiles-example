<%@include file="../taglib_includes.jsp" %>
<c:url value="/j_spring_security_logout" var="logoutUrl" />
    <div class="navbar navbar-inverse navbar-fixed-top">
      <div class="navbar-inner">
        <div class="container-fluid">
          <button type="button" class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse">
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <spring:url value="/index" var="homeUrl" htmlEscape="true" />
          		<a class="brand" href="${homeUrl}">SpringMVCTiles</a>
          <div class="nav-collapse collapse">
			<p class="navbar-text pull-right">
				<c:choose> 
		  			<c:when test="${pageContext.request.userPrincipal.name == null}">
		  				<spring:url value="/login" var="loginUrl" htmlEscape="true" />
		  				<a href="${loginUrl}">Login</a>
		  			</c:when>
		  			<c:otherwise>
		  				<spring:url value="javascript:formSubmit()" var="logoutUrl" htmlEscape="true" />
		  				<span>Welcome : ${pageContext.request.userPrincipal.name} | <a href="${logoutUrl}">Logout</a></span>
		  			</c:otherwise>
				</c:choose>
            </p>
            <ul class="nav">
				<c:choose> 
  					<c:when test="${currentSection eq 'index'}">
    					<li class="active">
  					</c:when>
  					<c:otherwise>
    					<li>
  					</c:otherwise>
				</c:choose>
            		<spring:url value="/index" var="homeUrl" htmlEscape="true" />
						<a href="${homeUrl}">Home</a></li>
              	<c:choose> 
  					<c:when test="${currentSection eq 'about'}">
    					<li class="active">
  					</c:when>
  					<c:otherwise>
    					<li>
  					</c:otherwise>
				</c:choose>
              		<spring:url value="/about" var="aboutUrl" htmlEscape="true" />
					<a href="${aboutUrl}">About</a></li>
              <!-- <li><a href="#contact">Contact</a></li> -->
            </ul>
          </div><!--/.nav-collapse -->
        </div>
      </div>
    </div>
    
<%-- Formerly
<div class="span-24">
	<img src="${pageContext.request.contextPath}/static/img/header.png" 
		width="950" style="padding-top:10px;" />
</div> --%>