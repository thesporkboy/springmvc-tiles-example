<%@include file="taglib_includes.jsp" %>
<div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
	<c:if test="${! empty title}">
		<h1 class="page-header">${title}</h1>
	</c:if>
	<c:if test="${! empty subtitle}">
		<h2 class="sub-header">${subtitle}</h2>
	</c:if>
	<c:if test="${! empty message}">
		<p>${message}</p>
	</c:if>
	
		<!-- The controller will stuff the page body in here. -->
		${tileBody}

</div>
