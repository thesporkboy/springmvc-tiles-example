<%@include file="taglib_includes.jsp"%>
<%@page session="true"%>

<!-- <div class="container"> -->
<div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">

	<form class="form-signin" name='loginForm'
		action="<c:url value='/j_spring_security_check' />" method='POST'>

		<c:if test="${! empty title}">
			<h2 class="form-signin-heading">${title}</h2>
		</c:if>
		<c:if test="${! empty subtitle}">
			<h4>${subtitle}</h4>
		</c:if>
		<c:if test="${! empty message}">
			<p>${message}</p>
		</c:if>
		
		<c:if test="${not empty error}">
			<div class="error">${error}</div>
		</c:if>
		<c:if test="${not empty msg}">
			<div class="msg">${msg}</div>
		</c:if>

		<label for="username" class="sr-only">User:</label>
		<input type="text" name='username' id="username" class="form-control" placeholder="username"
			required autofocus> 
		<label for="password" class="sr-only">Password</label>
		<input type="password" name='password' id="password" class="form-control"
			placeholder="Password" required>
		<!-- Figure out how to make this bit work. -->
		<!-- <div class="checkbox">
          <label>
            <input type="checkbox" value="remember-me"> Remember me
          </label>
        </div> -->
		<button class="btn btn-lg btn-primary btn-block" type="submit"
			name="submit">Sign in</button>
		<input type="hidden" name="${_csrf.parameterName}"
			value="${_csrf.token}" />
	</form>

</div><!-- /"col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main" -->